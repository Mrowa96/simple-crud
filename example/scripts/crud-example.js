window.addEventListener("load", function(){
    let nameInput = document.querySelector("#nameInput"),
        bikeInput = document.querySelector("#bikeInput"),
        crudTable = document.querySelector("#crudTable"),
        saveDataBtn = document.querySelector("#saveDataBtn");

    let crud = new Crud([nameInput, bikeInput], crudTable, {
        element: saveDataBtn,
        callback: function(response, inputs, isEdit) {
            var res = JSON.parse(response);

            return [
                inputs[0].value,
                res.bike_brand,
                res.bike_model,
                res.bike_frame_no
            ];
        },
        ajaxData: {
            url: 'server.php',
            toSend: {
                id: bikeInput
            }
        }
    });
});
