"use strict";

window.addEventListener("load", function () {
    var nameInput = document.querySelector("#nameInput"),
        bikeInput = document.querySelector("#bikeInput"),
        crudTable = document.querySelector("#crudTable"),
        saveDataBtn = document.querySelector("#saveDataBtn");

    var crud = new Crud([nameInput, bikeInput], crudTable, {
        element: saveDataBtn,
        callback: function callback(response, inputs, isEdit) {
            var res = JSON.parse(response);

            return [inputs[0].value, res.bike_brand, res.bike_model, res.bike_frame_no];
        },
        ajaxData: {
            url: 'server.php',
            toSend: {
                id: bikeInput
            }
        }
    });
});

//# sourceMappingURL=crud-example-compiled.js.map