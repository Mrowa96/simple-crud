class Crud {
    get elements() {
        return this._elements;
    }
    set elements(elements) {
        this._elements = elements;
    }

    get table() {
        return this._table;
    }
    set table(table) {
        this._table = table;
    }

    get states() {
        return this._states;
    }
    set states(states) {
        this._states = states;
    }

    get randomIds() {
        return this._randomIds;
    }
    set randomIds(id) {
        this._randomIds = id;
    }

    get save() {
        if (!this._save.element) {
            throw new Error("Save parameter lack of element property.");
        }
        else if (!this._save.event) {
            this._save.event = "click";
        }
        else if (!this._save.callback) {
            this._save.callback = () => {
            };
        }
        else if (!this._save.ajaxData) {
            this._save.ajaxData = false;
        }

        return this._save;
    }
    set save(save) {
        this._save = save;
    }

    constructor(elements, table, save) {
        if (!Array.isArray(elements) || elements.length === 0) {
            throw new Error("Elements must be instance of array and cannot be empty.");
        }
        else if (!(table instanceof HTMLElement)) {
            throw new Error("Table must be instance of HTMLElement and cannot be empty.");
        }
        else if (!(save instanceof Object)) {
            throw new Error("Save must be instance of Object and must have properties: element, event and callback.");
        }

        this.elements = elements;
        this.table = table;
        this.save = save;
        this.states = [];
        this.randomIds = [];

        this._handleSave();
    }

    _handleSave() {
        this.save.element.addEventListener(this.save.event, () => {
            /* Save state of input and bind to some id */
            let isEdit = !!(this.save.element.dataset.edit),
                state = {};

            if(!isEdit){
                state = {
                    id: this._randomId(),
                    data: []
                };

                for(let element of this.elements){
                    state.data.push({
                        element: element.id,
                        value : element.value
                    })
                }

                this.states[state.id] = state;
            }
            else{
                state = this.states[this.save.element.dataset.edit];
            }

            /* Run callback defined by user */
            if (this.save.ajaxData instanceof Object) {
                let promise = this._ajaxRequest(this.save.ajaxData);

                promise.then((response) => {
                    let data = this.save.callback(response, this.elements, isEdit);

                    (!isEdit) ? this._insertData(data, state) : this._updateData(data, state);
                }).catch(function (error) {
                    throw new Error(error);
                });
            }
            else if (this.save.ajaxData === false) {
                let data = this.save.callback(this.elements);

                (!isEdit) ? this._insertData(data, state) : this._updateData(data, state, isEdit);
            }
        })
    }

    _ajaxRequest(data) {
        return new Promise(function (resolve, reject) {
            let http = new XMLHttpRequest(),
                params = "?";

            for (let index in data.toSend) {
                if (data.toSend.hasOwnProperty(index)) {
                    params += index + "=" + data.toSend[index].value;
                }
            }

            http.open((data.method) ? data.method : "GET", data.url + params);
            http.onload = function () {
                if (http.status === 200) {
                    resolve(http.response);
                }
                else {
                    reject(new Error(http.statusText));
                }
            };

            http.onerror = function () {
                reject(new Error("Network error"));
            };

            http.send();
        });
    }

    _insertData(data, state) {
        if (Array.isArray(data)) {
            let row = new CrudRow(this.table),
                _self = this;

            row.open(state);
            row.add(data);
            row.addAction(state, "edit", function(){
                Crud._editEvent(_self, this);
            });
            row.addAction(state, "delete", function(){
                Crud._deleteEvent(_self, this);
            });
            row.close();
        }
        else {
            throw new Error("Data to save in the table must be instance of Array.");
        }
    }
    _updateData(data, state){
        if (Array.isArray(data)) {
            let row = new CrudRow(this.table);

            row.load(state);
            row.update(data);
            row.save();
        }
        else {
            throw new Error("Data to save in the table must be instance of Array.");
        }
    }

    _randomId(min = 1, max = 100000){
        let id = Math.floor((Math.random() * max) + min);

        if(this.randomIds.indexOf(id) === -1){
            this.randomIds.push(id);

            return id;
        }
        else{
            return this._randomId();
        }
    }

    static _editEvent(_self, button){
        let elements = _self.states[button.dataset.state].data;

        for(let data of elements){
            let domElement = document.querySelector("#" + data.element);

            domElement.value = data.value;
        }

        _self.save.element.dataset.edit = button.dataset.state;
    }
    static _deleteEvent(_self, button){
        let row = new CrudRow(_self.table),
            state = _self.states[button.dataset.state];

        row.load(state);
        row.remove();
    }
}
class CrudRow {
    get columns() {
        if (!this._columns) {
            throw new Error("Table requires minimum one column.")
        }

        return this._columns;
    }
    set columns(columns) {
        this._columns = columns;
    }

    get table() {
        return this._table;
    }
    set table(table) {
        this._table = table;
    }

    get row() {
        return this._row;
    }
    set row(row) {
        this._row = row;
    }

    constructor(table) {
        this.table = table;
        this.columns = this.table.querySelectorAll("thead th");
        this.toRowAppend = [];
    }

    add(data) {
        if (this.columns.length === data.length && this.row) {
            for(let cellData of data){
                let cell = document.createElement("td");

                cell.innerHTML = cellData;

                this.toRowAppend.push(cell);
            }
        }
    }
    update(data) {
        if (this.columns.length === data.length && this.row) {
            let cells = this.row.querySelectorAll("td"),
                index = 0;

            for(let cellData of data){
                cells[index].innerHTML = cellData;
                index++;
            }
        }
    }
    remove(){
        if(this.row){
            this.row.parentNode.removeChild(this.row);

            if(!this.table.querySelector("tbody tr")){
                let noDataRow = sessionStorage.getItem("noData");

                if(noDataRow){
                    this.table.querySelector("tbody").innerHTML = noDataRow;
                }
            }
        }
    }

    addAction(state, type, eventCallback) {
        let cell = document.createElement("td"),
            action, button;

        action = new CrudRowAction(state, type, eventCallback);
        button = action.create();

        cell.appendChild(button);

        this.toRowAppend.push(cell);
    }

    load(state){
        this.row = document.querySelector("#row" + state.id);
    }
    open(state){
        this.row = document.createElement("tr");
        this.row.id = "row" + state.id;

        if(this.table.querySelector(".noData")){
            let noData = this.table.querySelector(".noData");

            sessionStorage.setItem("noData", noData.parentNode.innerHTML);

            noData.parentNode.removeChild(noData);
        }
    }
    close(){
        if(this.row){
            this.toRowAppend.forEach((cell) => {
                if(cell instanceof HTMLElement){
                    this.row.appendChild(cell);
                }
            });

            this.save();
        }
    }
    save(){
        this.table.querySelector("tbody").appendChild(this.row);
    }
}
class CrudRowAction{
    constructor(state, type, eventCallback){
        this.state = state;
        this.type = type;
        this.eventCallback = eventCallback;
    }

    create(){
        return this._createButton();
    }

    _createButton(){
        let button = document.createElement("span");

        button.classList.add("btn", "btn-" + this.type);
        button.addEventListener("click", this.eventCallback);

        switch(this.type){
            case "edit":
                button.innerHTML = "Update";
                button.classList.add("btn-primary");
                button.dataset.state = this.state.id;
                break;
            case "delete":
                button.innerHTML = "Delete";
                button.classList.add("btn-danger");
                button.dataset.state = this.state.id;
                break;
        }

        return button;
    }
}