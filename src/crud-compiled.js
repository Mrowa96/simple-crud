"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Crud = function () {
    _createClass(Crud, [{
        key: "elements",
        get: function get() {
            return this._elements;
        },
        set: function set(elements) {
            this._elements = elements;
        }
    }, {
        key: "table",
        get: function get() {
            return this._table;
        },
        set: function set(table) {
            this._table = table;
        }
    }, {
        key: "states",
        get: function get() {
            return this._states;
        },
        set: function set(states) {
            this._states = states;
        }
    }, {
        key: "randomIds",
        get: function get() {
            return this._randomIds;
        },
        set: function set(id) {
            this._randomIds = id;
        }
    }, {
        key: "save",
        get: function get() {
            if (!this._save.element) {
                throw new Error("Save parameter lack of element property.");
            } else if (!this._save.event) {
                this._save.event = "click";
            } else if (!this._save.callback) {
                this._save.callback = function () {};
            } else if (!this._save.ajaxData) {
                this._save.ajaxData = false;
            }

            return this._save;
        },
        set: function set(save) {
            this._save = save;
        }
    }]);

    function Crud(elements, table, save) {
        _classCallCheck(this, Crud);

        if (!Array.isArray(elements) || elements.length === 0) {
            throw new Error("Elements must be instance of array and cannot be empty.");
        } else if (!(table instanceof HTMLElement)) {
            throw new Error("Table must be instance of HTMLElement and cannot be empty.");
        } else if (!(save instanceof Object)) {
            throw new Error("Save must be instance of Object and must have properties: element, event and callback.");
        }

        this.elements = elements;
        this.table = table;
        this.save = save;
        this.states = [];
        this.randomIds = [];

        this._handleSave();
    }

    _createClass(Crud, [{
        key: "_handleSave",
        value: function _handleSave() {
            var _this = this;

            this.save.element.addEventListener(this.save.event, function () {
                /* Save state of input and bind to some id */
                var isEdit = !!_this.save.element.dataset.edit,
                    state = {};

                if (!isEdit) {
                    state = {
                        id: _this._randomId(),
                        data: []
                    };

                    var _iteratorNormalCompletion = true;
                    var _didIteratorError = false;
                    var _iteratorError = undefined;

                    try {
                        for (var _iterator = _this.elements[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            var element = _step.value;

                            state.data.push({
                                element: element.id,
                                value: element.value
                            });
                        }
                    } catch (err) {
                        _didIteratorError = true;
                        _iteratorError = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion && _iterator.return) {
                                _iterator.return();
                            }
                        } finally {
                            if (_didIteratorError) {
                                throw _iteratorError;
                            }
                        }
                    }

                    _this.states[state.id] = state;
                } else {
                    state = _this.states[_this.save.element.dataset.edit];
                }

                /* Run callback defined by user */
                if (_this.save.ajaxData instanceof Object) {
                    var promise = _this._ajaxRequest(_this.save.ajaxData);

                    promise.then(function (response) {
                        var data = _this.save.callback(response, _this.elements, isEdit);

                        !isEdit ? _this._insertData(data, state) : _this._updateData(data, state);
                    }).catch(function (error) {
                        throw new Error(error);
                    });
                } else if (_this.save.ajaxData === false) {
                    var data = _this.save.callback(_this.elements);

                    !isEdit ? _this._insertData(data, state) : _this._updateData(data, state, isEdit);
                }
            });
        }
    }, {
        key: "_ajaxRequest",
        value: function _ajaxRequest(data) {
            return new Promise(function (resolve, reject) {
                var http = new XMLHttpRequest(),
                    params = "?";

                for (var index in data.toSend) {
                    if (data.toSend.hasOwnProperty(index)) {
                        params += index + "=" + data.toSend[index].value;
                    }
                }

                http.open(data.method ? data.method : "GET", data.url + params);
                http.onload = function () {
                    if (http.status === 200) {
                        resolve(http.response);
                    } else {
                        reject(new Error(http.statusText));
                    }
                };

                http.onerror = function () {
                    reject(new Error("Network error"));
                };

                http.send();
            });
        }
    }, {
        key: "_insertData",
        value: function _insertData(data, state) {
            var _this2 = this;

            if (Array.isArray(data)) {
                (function () {
                    var row = new CrudRow(_this2.table),
                        _self = _this2;

                    row.open(state);
                    row.add(data);
                    row.addAction(state, "edit", function () {
                        Crud._editEvent(_self, this);
                    });
                    row.addAction(state, "delete", function () {
                        Crud._deleteEvent(_self, this);
                    });
                    row.close();
                })();
            } else {
                throw new Error("Data to save in the table must be instance of Array.");
            }
        }
    }, {
        key: "_updateData",
        value: function _updateData(data, state) {
            if (Array.isArray(data)) {
                var row = new CrudRow(this.table);

                row.load(state);
                row.update(data);
                row.save();
            } else {
                throw new Error("Data to save in the table must be instance of Array.");
            }
        }
    }, {
        key: "_randomId",
        value: function _randomId() {
            var min = arguments.length <= 0 || arguments[0] === undefined ? 1 : arguments[0];
            var max = arguments.length <= 1 || arguments[1] === undefined ? 100000 : arguments[1];

            var id = Math.floor(Math.random() * max + min);

            if (this.randomIds.indexOf(id) === -1) {
                this.randomIds.push(id);

                return id;
            } else {
                return this._randomId();
            }
        }
    }], [{
        key: "_editEvent",
        value: function _editEvent(_self, button) {
            var elements = _self.states[button.dataset.state].data;

            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = elements[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var data = _step2.value;

                    var domElement = document.querySelector("#" + data.element);

                    domElement.value = data.value;
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            _self.save.element.dataset.edit = button.dataset.state;
        }
    }, {
        key: "_deleteEvent",
        value: function _deleteEvent(_self, button) {
            var row = new CrudRow(_self.table),
                state = _self.states[button.dataset.state];

            row.load(state);
            row.remove();
        }
    }]);

    return Crud;
}();

var CrudRow = function () {
    _createClass(CrudRow, [{
        key: "columns",
        get: function get() {
            if (!this._columns) {
                throw new Error("Table requires minimum one column.");
            }

            return this._columns;
        },
        set: function set(columns) {
            this._columns = columns;
        }
    }, {
        key: "table",
        get: function get() {
            return this._table;
        },
        set: function set(table) {
            this._table = table;
        }
    }, {
        key: "row",
        get: function get() {
            return this._row;
        },
        set: function set(row) {
            this._row = row;
        }
    }]);

    function CrudRow(table) {
        _classCallCheck(this, CrudRow);

        this.table = table;
        this.columns = this.table.querySelectorAll("thead th");
        this.toRowAppend = [];
    }

    _createClass(CrudRow, [{
        key: "add",
        value: function add(data) {
            if (this.columns.length === data.length && this.row) {
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                    for (var _iterator3 = data[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                        var cellData = _step3.value;

                        var cell = document.createElement("td");

                        cell.innerHTML = cellData;

                        this.toRowAppend.push(cell);
                    }
                } catch (err) {
                    _didIteratorError3 = true;
                    _iteratorError3 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                        }
                    } finally {
                        if (_didIteratorError3) {
                            throw _iteratorError3;
                        }
                    }
                }
            }
        }
    }, {
        key: "update",
        value: function update(data) {
            if (this.columns.length === data.length && this.row) {
                var cells = this.row.querySelectorAll("td"),
                    index = 0;

                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    for (var _iterator4 = data[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        var cellData = _step4.value;

                        cells[index].innerHTML = cellData;
                        index++;
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }
            }
        }
    }, {
        key: "remove",
        value: function remove() {
            if (this.row) {
                this.row.parentNode.removeChild(this.row);

                if (!this.table.querySelector("tbody tr")) {
                    var noDataRow = sessionStorage.getItem("noData");

                    if (noDataRow) {
                        this.table.querySelector("tbody").innerHTML = noDataRow;
                    }
                }
            }
        }
    }, {
        key: "addAction",
        value: function addAction(state, type, eventCallback) {
            var cell = document.createElement("td"),
                action = void 0,
                button = void 0;

            action = new CrudRowAction(state, type, eventCallback);
            button = action.create();

            cell.appendChild(button);

            this.toRowAppend.push(cell);
        }
    }, {
        key: "load",
        value: function load(state) {
            this.row = document.querySelector("#row" + state.id);
        }
    }, {
        key: "open",
        value: function open(state) {
            this.row = document.createElement("tr");
            this.row.id = "row" + state.id;

            if (this.table.querySelector(".noData")) {
                var noData = this.table.querySelector(".noData");

                sessionStorage.setItem("noData", noData.parentNode.innerHTML);

                noData.parentNode.removeChild(noData);
            }
        }
    }, {
        key: "close",
        value: function close() {
            var _this3 = this;

            if (this.row) {
                this.toRowAppend.forEach(function (cell) {
                    if (cell instanceof HTMLElement) {
                        _this3.row.appendChild(cell);
                    }
                });

                this.save();
            }
        }
    }, {
        key: "save",
        value: function save() {
            this.table.querySelector("tbody").appendChild(this.row);
        }
    }]);

    return CrudRow;
}();

var CrudRowAction = function () {
    function CrudRowAction(state, type, eventCallback) {
        _classCallCheck(this, CrudRowAction);

        this.state = state;
        this.type = type;
        this.eventCallback = eventCallback;
    }

    _createClass(CrudRowAction, [{
        key: "create",
        value: function create() {
            return this._createButton();
        }
    }, {
        key: "_createButton",
        value: function _createButton() {
            var button = document.createElement("span");

            button.classList.add("btn", "btn-" + this.type);
            button.addEventListener("click", this.eventCallback);

            switch (this.type) {
                case "edit":
                    button.innerHTML = "Update";
                    button.classList.add("btn-primary");
                    button.dataset.state = this.state.id;
                    break;
                case "delete":
                    button.innerHTML = "Delete";
                    button.classList.add("btn-danger");
                    button.dataset.state = this.state.id;
                    break;
            }

            return button;
        }
    }]);

    return CrudRowAction;
}();

//# sourceMappingURL=crud-compiled.js.map